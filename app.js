//@ts-check
const lib_conv = require('viva-convert')
const shared = require('vmst-shared')
const app_sql_store = require('./app_sql_store.js')
const app_sql_target = require('./app_sql_target.js')
const app_action = require('./app_action.js')
const app_timer = require('./app_timer.js')
const type = require('./@type.js')
const lib_vmst_driver = require('vmst-driver')

/** Examples - see _demo.js */
class App {
    /**
     * @param {type.constructor_options} options
     * @param {type.callback_error} callback
     * @param {type.callback_error} [callback_reload_actions]
     */
    constructor(options, callback, callback_reload_actions) {
        if (!lib_conv.isFunction(callback)) {
            throw new Error ('callback is empty')
        }
        try {
            let app_name = (lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.app_name, 'vmst-action'))
            let scope_name = (lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.scope_name, 'single-scope'))

            let check_app_name = (lib_conv.isEmpty(app_name) ? undefined : shared.check_app_name(app_name))
            if (!lib_conv.isEmpty(check_app_name)) {
                throw new Error(check_app_name)
            }
            let check_scope_name = (lib_conv.isEmpty(scope_name) ? undefined : shared.check_scope_name(scope_name))
            if (!lib_conv.isEmpty(check_scope_name)) {
                throw new Error(check_scope_name)
            }

            /** @type {type.private} */
            this._private = {
                app_name: app_name,
                scope_name: scope_name,
                local_timezone: (new Date()).getTimezoneOffset(),
                sql_store: {
                    sql: undefined,
                    connection_options: lib_conv.isAbsent(options) ? undefined : options.sql_connection_option_store,
                    timeout_reload_sec: lib_conv.isAbsent(options) ? undefined : lib_conv.toInt(options.time_reload_store_sec),
                    schema: lib_conv.toString(lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.schema_store)),
                    table: lib_conv.toString(lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.table_store))
                },
                sql_target: {
                    sql: undefined,
                    connection_options: lib_conv.isAbsent(options) ? undefined : options.sql_connection_option_target
                },
                action_render_list: []
            }

            app_sql_target.connect(this, error => {
                if (!lib_conv.isAbsent(error)) {
                    callback(error)
                    return
                }
                app_sql_store.connect(this, error => {
                    if (!lib_conv.isAbsent(error)) {
                        callback(error)
                        return
                    }

                    callback(error)

                    app_timer.load_store_start(this, error => {
                        callback_reload_actions(error)
                    })
                })
            })
        } catch (error) {
            callback(error)
        }
    }

    /**
     * @callback callback_store
     * @param {Error} error
     * @param {type.action_raw[]} action_raw_list
     */
    /**
     * @param {string|string[]} [rids]
     * @param {callback_store} callback
     */
    store_load (rids, callback) {
        try {
            app_sql_store.load(this, true, rids, (error, action_raw_list) => {
                callback(error, action_raw_list)
            })
        } catch(error) {
            callback(error, undefined)
        }
    }

    /**
     * @param {type.action_raw_edit[]} action_raw_list
     * @param {callback_store} callback
     */
    store_save (action_raw_list, callback) {
        try {
            app_sql_store.save(this, action_raw_list, error => {
                if (!lib_conv.isEmpty(error)) {
                    callback(error, undefined)
                    return
                }
                app_sql_store.load(this, true, action_raw_list.map(m => { return m.rid }), (error, action_raw_list) => {
                    callback(error, action_raw_list)
                })
            })
        } catch(error) {
            callback(error, undefined)
        }
    }

    /**
     * @callback callback_exec
     * @param {Error} error
     * @param {'preprocessor'|'exec'|'lock'|'postprocessor'} error_type
     * @param {lib_vmst_driver.type_exec_result_end} callback_exec
     * @param {Object} props
     */
    /**
     * @param {string} action_rid
     * @param {type.exec} [options]
     * @param {callback_exec} callback
    */
    exec (action_rid, options, callback) {
        try {
            if (lib_conv.isAbsent(options)) {
                options = {}
            }
        
            let rid = lib_conv.toString(action_rid, '')
            let trust_action_all = lib_conv.toBool(options.trust_action_all, true)
            let trust_action_list = lib_conv.toArray(options.trust_action_list, 'string')
        
            if (trust_action_all === false && !trust_action_list.some(f => lib_conv.equal(f,rid))) {
                callback(new Error (lib_conv.format('not found action "{0}"', rid)), 'preprocessor', undefined, undefined)
                return
            }
        
            let action = this._private.action_render_list.find(f => lib_conv.equal(f.rid, rid))

            if (lib_conv.isAbsent(action)) {
                callback(new Error (lib_conv.format('not found action "{0}"', rid)), 'preprocessor', undefined, undefined)
                return
            }
        
            if (!lib_conv.isAbsent(action.error)) {
                callback(new Error (lib_conv.format('action with name "{0}" has load errors', rid)), undefined, undefined, undefined)
                return
            }

            app_sql_target.exec_action(this, action, options.params, (error, error_type, callback_exec_end) => {
                if (!lib_conv.isAbsent(error)) {
                    callback(error, error_type, callback_exec_end, undefined)
                    return
                }

                try {
                    if (!lib_conv.isAbsent(callback_exec_end.error)) {
                        if (callback_exec_end.error_type === 'lock') {
                            callback(new Error(lib_conv.toString(action.sql_lock.message, 'operation was blocked by another process with the same lock')), 'lock', callback_exec_end, undefined)
                        } else {
                            callback(callback_exec_end.error, 'exec', callback_exec_end, undefined)
                        }
                        return
                    }

                    let props = {}

                    let process_table = false
                    let process_postprocessor = false

                    if (action.result.table_list.length > 0) {
                        process_table = true
                        if (action.result.table_list.length !== callback_exec_end.table_list.length) {
                            callback(
                                new Error (lib_conv.format("sql script return tables {0}, but expected tables {1}", [callback_exec_end.table_list.length, action.result.table_list.length])),
                                'postprocessor',
                                callback_exec_end,
                                undefined
                            )
                            return
                        }
                        action.result.table_list.forEach((t,i) => {
                            props[t] = callback_exec_end.table_list[i].row_list
                        })
                    }

                    if (!lib_conv.isAbsent(action.result.postprocessor)) {
                        process_postprocessor = true
                        try {
                            props = action.result.postprocessor(callback_exec_end, props)
                        } catch (error) {
                            callback(
                                error,
                                'postprocessor',
                                callback_exec_end,
                                undefined
                            )
                            return
                        }
                    }

                    if (process_table === false && process_postprocessor === false) {
                        props = callback_exec_end.table_list.map(m => { return m.row_list })
                    }

                    callback(undefined, undefined, callback_exec_end, props)
                } catch (error) {
                    callback(error, undefined, callback_exec_end, undefined)
                }
            })
        } catch (error) {
            callback(error, undefined, undefined, undefined)
        }
    }
}

module.exports = App