//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_vmst_driver = require('vmst-driver')
const lib_vmst_helper = require('vmst-helper')
const shared = require('vmst-shared')
const app = require('./app.js')
const action = require('./app_action.js')
const app_sql_shared = require('./app_sql_shared.js')
const type = require('./@type.js')
const helper = require('vmst-helper')

exports.connect = connect
exports.load_table_schema = load_table_schema
exports.exec_action = exec_action

/**
 * @param {app} app
 * @param {type.callback_error} callback
 */
function connect(app, callback) {
    app_sql_shared.connect(app._private.sql_target.connection_options, app._private.app_name, app._private.scope_name, (error, sql) => {
        if (!lib_conv.isAbsent(error)) {
            callback(error)
            return
        }
        app._private.sql_target.sql = sql
        callback(undefined)
    })
}

/**
 * @callback callback_load_store_regular_table_schema
 * @param {Error} error
 * @param {type.regular_table_replacement[]} regular_table_list
 *//**
 * @param {app} app
 * @param {type.regular_table_replacement[]} regular_table_list
 * @param {callback_load_store_regular_table_schema} callback
 */
function load_table_schema(app, regular_table_list, callback) {
    if (regular_table_list.length <= 0 || lib_conv.isAbsent(app._private.sql_target.sql)) {
        callback(undefined, regular_table_list)
        return
    }
    /** @type {string[]} */
    let database_list = []
    regular_table_list.forEach(regular_table => {
        if (database_list.includes(regular_table.database)) return
        database_list.push(regular_table.database)
    })

    /** @type {string[]} */
    let query_total = []

    database_list.forEach(database => {
        let table_list = []

        regular_table_list.filter(f => f.database === database).forEach(table => {
            if (table_list.some(f => f.schema === table.schema && f.table === table.table)) return
            table_list.push({schema: table.schema, table: table.table})
        })

        query_total.push(
            [
                "USE ".concat(lib_conv.border_add(database, "[", "]")),
                ";with need_tables AS (",
                table_list.map(m => {
                    return lib_conv.format("    SELECT '{0}' [schema], '{1}' [table]", [m.schema, m.table])
                }).join(" UNION ALL".concat(lib_os.EOL)),
                ")",
                "SELECT c.TABLE_SCHEMA, c.TABLE_NAME, c.COLUMN_NAME, c.IS_NULLABLE, c.DATA_TYPE, c.CHARACTER_MAXIMUM_LENGTH, c.NUMERIC_PRECISION, c.NUMERIC_SCALE, prop_column.[value] COLUMN_DESCRIPTION, prop_table.[value] TABLE_DESCRIPTION, pk.ORDINAL_POSITION PRIMARY_ORDINAL_POSITION",
                "FROM INFORMATION_SCHEMA.[COLUMNS] c",
                "JOIN need_tables n ON n.[schema] = c.TABLE_SCHEMA AND n.[table] = c.TABLE_NAME",
                "OUTER APPLY fn_listextendedproperty(default, 'SCHEMA', c.TABLE_SCHEMA, 'TABLE', c.TABLE_NAME, null, null) prop_table",
                "OUTER APPLY fn_listextendedproperty(default, 'SCHEMA', c.TABLE_SCHEMA, 'TABLE', c.TABLE_NAME, 'COLUMN', c.COLUMN_NAME) prop_column",
                "LEFT JOIN (",
                    "SELECT tc.TABLE_SCHEMA, tc.TABLE_NAME, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION",
                    "FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc",
                    "JOIN need_tables n ON n.[schema] = tc.TABLE_SCHEMA AND n.[table] = tc.TABLE_NAME",
                    "JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu ON kcu.TABLE_SCHEMA =  tc.TABLE_SCHEMA AND kcu.TABLE_NAME =  tc.TABLE_NAME",
                    "WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY'",
                ") pk ON pk.TABLE_SCHEMA = c.TABLE_SCHEMA AND pk.TABLE_NAME = c.TABLE_NAME AND pk.COLUMN_NAME = c.COLUMN_NAME",
                "ORDER BY c.TABLE_SCHEMA, c.TABLE_NAME, c.ORDINAL_POSITION"
            ].join(lib_os.EOL)
        )
    })

    app._private.sql_target.sql.exec(query_total, {stop_on_error: false}, callback_exec => {
        if (callback_exec.type !== 'end') return
        if (!lib_conv.isAbsent(callback_exec.end.error)) {
            callback(callback_exec.end.error, undefined)
            return
        }
        try {
            database_list.forEach((database, index) => {
                let error = callback_exec.end.query_list.find(f => f.query_index === index).error
                if (!lib_conv.isAbsent(error)) {
                    regular_table_list.filter(f => f.database === database).forEach(regular_table => {regular_table.error = error})
                    return
                }
                regular_table_list.filter(f => f.database === database).forEach(regular_table => {

                    callback_exec.end.table_list
                        .find(f => f.query_index === index).row_list
                        .filter(f => lib_conv.equal(regular_table.schema, lib_conv.findPropertyValueInObject(f, 'TABLE_SCHEMA')) && lib_conv.equal(regular_table.table, lib_conv.findPropertyValueInObject(f, 'TABLE_NAME')) ).forEach((row, index) => {
                            if (index === 0) {
                                regular_table.description = lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'TABLE_DESCRIPTION'))
                            }

                            let sql_type = lib_vmst_helper.helper_get_types_sql().find(f => lib_conv.equal(f.type, lib_conv.findPropertyValueInObject(row, 'DATA_TYPE')))
                            /** @type {'max'|number} */
                            let len_chars = lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'CHARACTER_MAXIMUM_LENGTH'))
                            if (len_chars === -1) {
                                len_chars = 'max'
                            }

                            regular_table.column_list.push({
                                name: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'COLUMN_NAME'), ''),
                                type: sql_type.type,
                                nullable: (regular_table.strong === false ? true : lib_conv.toBool(lib_conv.findPropertyValueInObject(row, 'IS_NULLABLE'), false)),
                                len_chars: (regular_table.strong === false && sql_type.len === 'allow' ? 'max' : len_chars),
                                precision: (sql_type.precision === 'allow' ? lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'NUMERIC_PRECISION')) : undefined),
                                scale: (sql_type.scale === 'allow' ? lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'NUMERIC_SCALE')) : undefined),
                                description: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'COLUMN_DESCRIPTION')),
                                pk_position: lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'PRIMARY_ORDINAL_POSITION'))
                            })
                    })

                })
            })
            callback(undefined, regular_table_list)
        } catch (error) {
            callback(error, regular_table_list)
        }
    })
}

/**
 * @callback callback_exec
 * @param {Error} error
 * @param {'preprocessor'|'exec'|'lock'} error_type
 * @param {lib_vmst_driver.exec_result_end} callback_exec
 */

/**
 * @param {app} app
 * @param {type.action_render} action
 * @param {Object} params
 * @param {callback_exec} callback
 */
function exec_action(app, action, params, callback) {
    try {
        let query = []
        for (let i = 0; i < action.sql_param_list.length; i++) {
            let sql_param = action.sql_param_list[i]
            if (sql_param.type === 'table') {
                let columns_query = sql_param.table.column_list.map(m => { return "".concat(
                    lib_conv.border_add(m.name,"[","]"),
                    " ",
                    helper.helper_get_declare(m.type, m.precision, m.scale, m.len_chars, 'char').sql,
                    (m.nullable === false ? " NOT NULL" : "")
                ) }).join(",")
                if (sql_param.table.column_list.some(f => !lib_conv.isEmpty(f.pk_position))) {
                    columns_query = columns_query.concat(", PRIMARY KEY (",
                        sql_param.table.column_list.filter(f => !lib_conv.isEmpty(f.pk_position)).sort((a, b) => {
                            if (a.pk_position > b.pk_position) return 1
                            if (a.pk_position < b.pk_position) return -1
                            return 0
                        }).map(m => { return lib_conv.border_add(m.name, "[", "]") }).join(",")
                    ,")")
                }
                if (sql_param.table.name.substring(0, 1) === '#') {
                    query.push(lib_conv.format("IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}", sql_param.table.name))
                    query.push(lib_conv.format("CREATE TABLE {0}({1})", [sql_param.table.name, columns_query]))
                } else if (sql_param.table.name.substring(0, 1) === '@') {
                    query.push(lib_conv.format("DECLARE {0} TABLE ({1})", [sql_param.table.name, columns_query]))
                }
                let value_raw = lib_conv.findPropertyValueInObject(params, sql_param.table.js_name)
                if (lib_conv.isAbsent(value_raw)) {
                    continue
                }
                if (!Array.isArray(value_raw)) {
                    callback(
                        new Error(lib_conv.format('for action "{0}" required ARRAY parameter "{1}"{2}', [
                            action.rid,
                            sql_param.table.js_name,
                            (lib_conv.isEmpty(sql_param.table.description) ? '' : ' - "'.concat(sql_param.table.description, '"'))
                        ])),
                        'preprocessor',
                        undefined,
                    )
                    return
                }
                query.push(lib_conv.format("INSERT INTO {0} ({1})", [sql_param.table.name, sql_param.table.column_list.map(m => { return lib_conv.border_add(m.name, '[',']') }).join(",") ]))
                query.push(
                    value_raw.map(item_raw => { return "SELECT "
                            .concat(
                                sql_param.table.column_list.map(m => { return helper.helper_js_to_sql(lib_conv.findPropertyValueInObject(item_raw, m.name), m.type, true) }).join(",")
                            )
                    }).join(" UNION ALL ".concat(lib_os.EOL))
                )
            }
            
            if (sql_param.type === 'scalar') {
                let declare = helper.helper_get_declare(sql_param.scalar.type, sql_param.scalar.precision, sql_param.scalar.scale, sql_param.scalar.len_chars, 'char')
                let value_raw = lib_conv.findPropertyValueInObject(params, sql_param.scalar.js_name)
                let value = 'NULL'
                if (!lib_conv.isAbsent(value_raw)) {
                    value = helper.helper_js_to_sql(value_raw, sql_param.scalar.type, true)
                } else if (sql_param.scalar.nullable === false) {
                    callback(
                        new Error(lib_conv.format('for action "{0}" required parameter "{1}"{2}', [
                            action.rid,
                            sql_param.scalar.js_name,
                            (lib_conv.isEmpty(sql_param.scalar.description) ? '' : ' - "'.concat(sql_param.scalar.description, '"'))
                        ])),
                        'preprocessor',
                        undefined,
                    )
                    return
                }
                query.push(lib_conv.format("DECLARE {0} {1}; SET {0} = {2}", [sql_param.scalar.name, declare.sql, value]))
            }        
        }
        query.push(action.sql_script)

        app._private.sql_target.sql.exec(query.join(lib_os.EOL), {lock: {key: action.sql_lock.key, wait: action.sql_lock.wait} }, callback_exec => {
            if (callback_exec.type !== 'end') return
            callback(undefined, undefined, callback_exec.end)
        })
    } catch (error) {
        callback(error, undefined, undefined)
    }
}