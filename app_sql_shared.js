//@ts-check
const lib_conv = require('viva-convert')
const lib_vmst_driver = require('vmst-driver')
const type = require('./@type.js')
const app = require('./app.js')

exports.connect = connect
/**
 * @callback callback_connect
 * @param {Error} error
 * @param {lib_vmst_driver} sql
 */
/**
 * @param {type.sql_connection_options} connection_options
 * @param {string} app_name
 * @param {string} scope_name
 * @param {callback_connect} callback
 */
function connect(connection_options, app_name, scope_name, callback) {
    if (lib_conv.isAbsent(connection_options)) {
        callback(new Error("connection_options = undefined"), undefined)
        return
    }
    if (lib_conv.isAbsent(connection_options.instance)) {
        callback(new Error("connection_options.instance = undefined"), undefined)
        return
    }

    let sql = new lib_vmst_driver({
        instance: connection_options.instance,
        login: connection_options.login,
        password: connection_options.password,
        additional: {
            database: connection_options.database,
            app_name: lib_conv.format("{0}.{1}", [app_name, scope_name])
        }
    })

    sql.ping(error => {
        if (!lib_conv.isAbsent(error)) {
            callback(error, undefined)
            return
        }
        callback(undefined, sql)
    })
}