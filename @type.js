// @ts-check
exports.stub = stub
function stub () {}

const lib_vmst_driver = require('vmst-driver')
const lib_vmst_helper = require('vmst-helper')

/**
 * @callback callback_error
 * @param {Error} error
 */

/**
 * @typedef constructor_options
 * @property {string} [app_name]
 * @property {string} [scope_name]
 * @property {sql_connection_options} [sql_connection_option_target]
 * @property {sql_connection_options} [sql_connection_option_store]
 * @property {string} [schema_store] table schema name for storage actions, default - 'vmst'
 * @property {string} [table_store] table name for storage actions, default - 'action'
 * @property {number} [time_reload_store_sec]
 */

/**
 * @typedef sql_connection_options
 * @property {string} instance ms sql instance, examples - 'localhost', 'myserver/myinstance'
 * @property {string} [login] login for ms sql authentication, if need domain authentication, set undefined
 * @property {string} [password] password for ms sql authentication, if need domain authentication, set undefined
 * @property {string} [database] database name for exec action, default - 'tempdb'
 */

/**
 * @typedef private
 * @property {string} app_name
 * @property {string} scope_name
 * @property {number} local_timezone
 * @property {sql_store} sql_store
 * @property {sql_target} sql_target
 * @property {action_render[]} action_render_list
 */

/**
 * @typedef sql_target
 * @property {lib_vmst_driver} sql
 * @property {sql_connection_options} connection_options
 */

/**
 * @typedef sql_store
 * @property {lib_vmst_driver} sql
 * @property {sql_connection_options} connection_options
 * @property {number} timeout_reload_sec
 * @property {string} [schema] table schema name for storage actions, default - 'vmst'
 * @property {string} [table] table name for storage actions, default - 'action'
 */

/**
 * @typedef action_raw
 * @property {string} rid
 * @property {string} rid_origin
 * @property {string} title
 * @property {string} note
 * @property {string} sql_script
 * @property {string} sql_param
 * @property {string} sql_param_note
 * @property {string} sql_lock_key
 * @property {number} sql_lock_wait
 * @property {string} sql_lock_message
 * @property {string[]} tag_list
 * @property {string} result_table
 * @property {string} result_postprocessor
 * @property {action_eav[]} eav_list
 * @property {action_render} [render]
 */

/**
 * @typedef action_eav
 * @property {string} rid
 * @property {string} data
 */

/**
 * @typedef action_raw_edit_internal
 * @property {'delete'|'save'} [edit_action]
 */

/**
 * @typedef {action_raw & action_raw_edit_internal} action_raw_edit
 */

/**
 * @typedef action_render
 * @property {Error} [error]
 * @property {string} rid
 * @property {string} title
 * @property {string} note
 * @property {string} sql_script
 * @property {sql_param[]} sql_param_list
 * @property {sql_lock} sql_lock
 * @property {action_render_result} result
 * @property {action_eav[]} eav_list
 */

/**
 * @typedef action_render_result
 * @property {string[]} table_list
 * @property {function} postprocessor
 */

// * @property {string[]} sql_result_list

/**
 * @typedef type_column_additional
 * @property {string} js_name
 * @property {string} [description_from_regular]
 */

/**
 * @typedef {lib_vmst_helper.type_column & type_column_additional} type_column_extra
 */

/**
 * @typedef sql_param
 * @property {'scalar'|'table'} type
 * @property {type_column_extra} [scalar]
 * @property {sql_param_table} [table]
 */

/**
 * @typedef sql_param_table
 * @property {string} name
 * @property {string} js_name
 * @property {string} description
 * @property {string} [description_from_regular]
 * @property {type_column_extra[]} column_list
 */

/**
 * @typedef sql_lock
 * @property {number} wait
 * @property {string} message
 * @property {string} key
 */

/**
 * @typedef regular_table_replacement
 * @property {string} database
 * @property {string} schema
 * @property {string} table
 * @property {string} description
 * @property {boolean} strong
 * @property {string} column_name
 * @property {lib_vmst_helper.type_column[]} column_list
 * @property {Error} [error]
 */

/**
 * @typedef exec
 * @property {Object} [params]
 * @property {boolean} [trust_action_all]
 * @property {string[]} [trust_action_list]
 */