//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_vmst_driver = require('vmst-driver')
const helper = require('vmst-helper')
const shared = require('vmst-shared')
const app = require('./app.js')
const app_sql_store = require('./app_sql_store.js')
const app_sql_target = require('./app_sql_target.js')
const type = require('./@type.js')

exports.convert = convert

/**
 * @callback callback_convert_list
 * @param {Error} error
 * @param {type.action_render[]} action_render_list
 */
/**
 * @param {app} app
 * @param {type.action_raw[]} action_raw_list
 * @param {callback_convert_list} callback
 */
function convert(app, action_raw_list, callback) {
    if (lib_conv.isAbsent(action_raw_list)) {
        callback(undefined, [])
        return
    }

    /** @type {type.regular_table_replacement[]} */
    let regular_table_list = []

    action_raw_list.forEach(raw => {
        try {
            raw.render = convert_one_raw(raw)
            if (lib_conv.isAbsent(raw.render.error)) {
                raw.render.sql_param_list.forEach(param => {
                    if (param.type !== 'table') return
                    param.table.column_list.forEach(column => {
                        if (lib_conv.isEmpty(column.name) || column.name.substring(0, 1) !== '*') return
                        regular_table_list.push(parse_regular_table_name(column.name, app._private.sql_target.connection_options.database, 'dbo'))
                    })
                })
            }
        } catch (error) {
            if (!lib_conv.isAbsent(raw) && !lib_conv.isEmpty(raw.rid)) {
                raw.render = {
                    error: error,
                    rid: raw.rid,
                    title: '',
                    note: '',
                    sql_lock: undefined,
                    sql_param_list: [],
                    sql_script: '',
                    eav_list: [],
                    result:{table_list: [], postprocessor:function(callback_exec, props){}}
                }
            }
        }
    })

    app_sql_target.load_table_schema(app, regular_table_list, (error, regular_table_list) => {
        if (!lib_conv.isAbsent(error)) {
            callback(error, [])
            return
        }

        regular_table_list.forEach(regular_table => {
            action_raw_list.filter(f => !lib_conv.isAbsent(f.render) && lib_conv.isAbsent(f.render.error)).map(m => { return m.render }).forEach(action_render => {
                action_render.sql_param_list.forEach(param => {
                    if (param.type !== 'table') return
                    if (!param.table.column_list.some(f => lib_conv.equal(f.name, regular_table.column_name))) return
                    if (!lib_conv.isAbsent(regular_table.error)) {
                        action_render.error = new Error (lib_conv.toErrorMessage(regular_table.error, 'in get schema for regular table "{0}"', regular_table.column_name, 'message'))
                    }
                    param.table.description_from_regular = regular_table.description
                    /** @type {type.type_column_extra[]} */
                    let column_list = []
                    param.table.column_list.reverse().forEach(column => {
                        if (lib_conv.equal(column.name, regular_table.column_name)) {
                            regular_table.column_list.reverse().forEach(column_regular => {
                                if (!column_list.some(f => lib_conv.equal(f.name, column_regular.name))) {
                                    column_list.unshift({...column_regular, description: undefined, js_name: js_name(column_regular.name), description_from_regular: column_regular.description})
                                }
                            })
                        } else {
                            if (!column_list.some(f => lib_conv.equal(f.name, column.name))) {
                                column_list.unshift(column)
                            }
                        }
                    })
                    param.table.column_list = column_list
                })
            })
        })
        action_raw_list.filter(f => !lib_conv.isAbsent(f.render) && lib_conv.isAbsent(f.render.error)).map(m => { return m.render }).forEach(action_render => {
            let dublicate_param_name_list = lib_conv.duplicates(action_render.sql_param_list.map(m => { return (m.type === 'table' ? m.table.js_name : (m.type === 'scalar' ? m.scalar.js_name : '')) } ))
            if (dublicate_param_name_list.length > 0) {
                action_render.error = new Error(lib_conv.format('param(s) with name(s) "{0}" occur more than once', dublicate_param_name_list.join('","') ))
            } else {
                action_render.sql_param_list.filter(f => f.type === 'table').forEach(param => {
                    let dublicate_param_name_list = lib_conv.duplicates(param.table.column_list.map(m => { return m.js_name }))
                    if (dublicate_param_name_list.length > 0) {
                        action_render.error = new Error(lib_conv.format('in table "{0}" param(s) with name(s) "{1}" occur more than once', [param.table.name, dublicate_param_name_list.join('","')] ))
                    }
                })
            }
        })

        action_raw_list.filter(f => !lib_conv.isAbsent(f.render) && !lib_conv.isEmpty(f.sql_param_note)).forEach(raw => {
            convert_sql_param_note_list(lib_conv.toString(raw.sql_param_note, '').trim(), raw.render.sql_param_list)
        })

        callback(undefined, action_raw_list.filter(f => !lib_conv.isAbsent(f.render)).map(m => { return m.render }))
    })
}

/**
 * @param {type.action_raw} raw
 * @return {type.action_render}
 */
function convert_one_raw(raw) {
    if (lib_conv.isAbsent(raw)) {
        throw new Error ('raw is empty')
    }

    let sql_lock_key = lib_conv.toString(raw.sql_lock_key)
    let sql_lock_message = lib_conv.toString(raw.sql_lock_message)
    let sql_lock_wait = lib_conv.toInt(raw.sql_lock_wait)
    if (!(
        (lib_conv.isEmpty(sql_lock_key) && lib_conv.isEmpty(sql_lock_message) && lib_conv.isEmpty(sql_lock_wait))
        ||
        (!lib_conv.isEmpty(sql_lock_key) && !lib_conv.isEmpty(sql_lock_message) && !lib_conv.isEmpty(sql_lock_wait))
    )) {
            throw new Error ('params sql_lock_key,sql_lock_message,sql_lock_wait must be either all empty or all non-empty')
    }

    /** @type {type.action_render} */
    let result = {
        rid: lib_conv.toString(raw.rid, '').trim().toLowerCase(),
        title: lib_conv.toString(raw.title, '').trim(),
        note: lib_conv.toString(raw.note, '').trim(),
        sql_script: lib_conv.toString(raw.sql_script, '').trim(),
        sql_param_list: convert_sql_param_list(raw.sql_param),
        result: convert_result(raw.result_table, raw.result_postprocessor),
        //js_postprocessor: convert_js_postprocessor(raw.sql_result),
        sql_lock: {
            key: sql_lock_key,
            message: sql_lock_message,
            wait: sql_lock_wait
        },
        eav_list: raw.eav_list
    }

    if (lib_conv.isEmpty(result.rid)) {
        throw new Error ('action rid is empty')
    }

    return result
}

/**
 * @param {string} s
 * @returns {string}
 */
function string_bad_symbol_to_space(s) {
    [
        lib_os.EOL,                 //enter
        String.fromCharCode(160),   //non-breaking space
        String.fromCharCode(9),     //tab
    ].forEach(bad_symb => {
        s = lib_conv.replaceAll(s, bad_symb, ' ')
    })
    return s
}

/**
 * @param {string} s
 * @returns {string}
 */
function string_normalize(s) {
    s = string_bad_symbol_to_space(s)
    let repl = [
        "(",
        ")",
        ",",
        ";",
        "'",
        '"'
    ]
    repl.forEach(fnd => {
        s = lib_conv.replaceAll(s, fnd, ' '.concat(fnd, ' '))
    })
    return lib_conv.replaceAll(s, '  ', ' ', true).trim()
}

/**
 * @param {string} sql_params
 * @returns {string[]}
 */
function convert_sql_param_list_raw(sql_params) {
    let result = []
    let bracked = 0
    let i_start = 0

    let lexems = string_normalize(sql_params).split(' ')
    if (lexems.length > 0) {
        if (lexems[0] !== ',') {
            lexems.unshift(',')
        }
        if (lexems[lexems.length - 1] !== ',') {
            lexems.push(',')
        }
    }
    lexems.forEach((l, i) => {
        if (i === 0) return
        if (l === '(') bracked++
        if (l === ')') bracked--
        if (bracked < 0) {
            throw new Error (lib_conv.format('find inexplicit lexem ")" in param string "{0}" ', lexems.slice(i_start + 1, i + 1).join(' ')))
        }
        if ([',',';'].includes(l) && bracked === 0) {
            i_start++
            result.push(lexems.slice(i_start, i).map(m => { return m}).join(' '))
            i_start = i
        }
    })
    return result
}

/**
 * @param {string} sql_params
 * @param {boolean} [is_table]
 * @returns {type.sql_param[]}
 */
function convert_sql_param_list (sql_params, is_table) {
    /** @type {type.sql_param[]} */
    let sql_param_list = []

    convert_sql_param_list_raw(sql_params).forEach(param_raw => {
        let lexems = param_raw.split(' ')

        /** @type {type.sql_param} */
        let sql_param = {
            type: undefined
        }

        // check name
        let name = (lexems.length > 0 ? lexems[0] : '').trim()
        if (lib_conv.isEmpty(name)) {
            throw new Error (lib_conv.format('in param string "{0}" not find name', param_raw))
        }
        lexems = lexems.splice(1, lexems.length)

        if (lexems.length === 0 && is_table === true && name.substring(0, 1) === '*') {
            sql_param_list.push({type: 'scalar', scalar: {name: name, type: undefined, description: '', js_name: undefined}})
            return
        }

        // check type
        let type_param = (lexems.length > 0 ? lexems[0] : '').trim().toLowerCase()
        // if type is table
        if (lib_conv.equal(type_param, 'table')) {
            if (!['#', '@'].includes(name.substring(0 , 1))) {
                throw new Error (lib_conv.format('in table param string "{0}" - allowed only temp table (prefix "#" or "##") or table variable (prefix "@")', param_raw))
            }
            sql_param.type = 'table'

            sql_param.table = {name: name, description: '', column_list: [], js_name: js_name(name)}
            lexems = lexems.splice(1, lexems.length)
            if (lexems.length > 0 && lexems[0] === '(') {
                lexems = lexems.splice(1, lexems.length)
            } else {
                throw new Error (lib_conv.format('in param string "{0}" not find "(" after type "table"', param_raw))
            }
            if (lexems.length > 0 && lexems[lexems.length - 1] === ')') {
                lexems = lexems.splice(0, lexems.length - 1)
            } else {
                throw new Error (lib_conv.format('in param string "{0}" not find ")" before columns for type "table"', param_raw))
            }

            try {
                let table_param_list = convert_sql_param_list(lexems.join(' '), true)
                if (table_param_list.length < 0) {
                    throw new Error(lib_conv.format('in param table "{0}" not find columns', param_raw))
                }
                sql_param.table.column_list = table_param_list.map(m => { return m.scalar })
                sql_param_list.push(sql_param)
                return
            } catch (error) {
                throw new Error(lib_conv.format('in param table "{0}" {1}', [param_raw, error.message]))
            }
        }

        // if type is not table
        if (lib_conv.equal(type_param, 'guid')) {
            type_param = 'uniqueidentifier'
        } else if (lib_conv.equal(type_param, 'boolean')) {
            type_param = 'bit'
        } else if (lib_conv.equal(type_param, 'string')) {
            type_param = 'nvarchar'
        } else if (lib_conv.equal(type_param, 'number')) {
            type_param = 'float'
        }
        let sql_type = helper.helper_get_types_sql().find(f => lib_conv.equal(f.type, type_param))
        if (lib_conv.isAbsent(sql_type)) {
            throw new Error (lib_conv.format('in param string "{0}" not find known sql type', param_raw))
        }
        if (name.substring(0, 1) !== '@' && is_table !== true) {
            throw new Error (lib_conv.format('in scalar param string "{0}" not find "@" as first char', param_raw))
        }
        lexems = lexems.splice(1, lexems.length)

        sql_param.type = 'scalar'
        sql_param.scalar = {
            name: name,
            js_name: js_name(name),
            // @ts-ignore
            type: type_param
        }

        if (lexems.length > 1 && lib_conv.equal(lexems[lexems.length - 2], 'not') && lib_conv.equal(lexems[lexems.length - 1], 'null')) {
            sql_param.scalar.nullable = false
            lexems = lexems.splice(0, lexems.length - 2)
        } else if (lexems.length > 0 && lib_conv.equal(lexems[lexems.length - 1], 'null')) {
            sql_param.scalar.nullable = true
            lexems = lexems.splice(0, lexems.length - 1)
        } else {
            sql_param.scalar.nullable = true
        }

        if (lexems.length === 3 && lexems[0] === '(' && (lib_conv.equal(lexems[1], 'max') || lib_conv.toInt(lexems[1], 0) > 0) && lexems[2] === ')') {
            sql_param.scalar.len_chars = (lib_conv.equal(lexems[1], 'max') ? 'max' : lib_conv.toInt(lexems[1]))
        } else if (lexems.length === 5 && lexems[0] === '(' && lib_conv.toInt(lexems[1],0) > 0 && lexems[2] === ',' && lib_conv.toInt(lexems[3],0) > 0 && lexems[4] === ')') {
            sql_param.scalar.precision = lib_conv.toInt(lexems[1])
            sql_param.scalar.scale = lib_conv.toInt(lexems[3])
        } else if (lexems.length > 0) {
            throw new Error (lib_conv.format('find inexplicit substring "{0}" in param string "{1}" ', [lexems.join(' '), param_raw]))
        }

        if (
            (!lib_conv.isEmpty(sql_param.scalar.len_chars) && sql_type.len !== 'allow' && sql_type.len !== 'deny_max') ||
            (sql_param.scalar.len_chars === 'max' && sql_type.len !== 'allow')
            ) {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" string length "{2}" cannot be specified', [param_raw, sql_param.scalar.type, sql_param.scalar.len_chars]))
        }

        if (lib_conv.isEmpty(sql_param.scalar.len_chars) && (sql_type.len === 'allow' || sql_type.len === 'deny_max')) {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" string length must be specified', [param_raw, sql_param.scalar.type]))
        }

        if (!lib_conv.isEmpty(sql_param.scalar.precision) && sql_type.precision !== 'allow') {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" precision "{2}" cannot be specified', [param_raw, sql_param.scalar.type, sql_param.scalar.precision]))
        }

        if (!lib_conv.isEmpty(sql_param.scalar.scale) && sql_type.scale !== 'allow') {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" scale "{2}" cannot be specified', [param_raw, sql_param.scalar.type, sql_param.scalar.scale]))
        }

        if (lib_conv.isEmpty(sql_param.scalar.precision) && sql_type.precision === 'allow') {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" precision must be specified', [param_raw, sql_param.scalar.type]))
        }

        if (lib_conv.isEmpty(sql_param.scalar.scale) && sql_type.scale === 'allow') {
            throw new Error (lib_conv.format('in param string "{0}" for type "{1}" scale must be specified', [param_raw, sql_param.scalar.type]))
        }

        if (!lib_conv.isEmpty(sql_param.scalar.precision) && !lib_conv.isEmpty(sql_param.scalar.scale) && sql_param.scalar.scale > sql_param.scalar.precision) {
            throw new Error (lib_conv.format('in param string "{0}" scale "{1}" must not be more than precision "{2}"', [param_raw, sql_param.scalar.scale, sql_param.scalar.precision]))
        }

        if (!lib_conv.isEmpty(sql_param.scalar.name) && name.substring(0, 1) === '#') {
            throw new Error (lib_conv.format('in param string "{0}" scalar param with temporary table name "{1}"', [param_raw, name]))
        }

        sql_param_list.push(sql_param)
    })

    return sql_param_list
}

/**
 * @param {string} result_table
 * @param {string} result_postprocessor
 * @returns {type.action_render_result}
 */
function convert_result(result_table, result_postprocessor) {
    let table_list = []
    let postprocessor = undefined
    if (!lib_conv.isEmpty(result_table)) {
        table_list = result_table.split(/[,;]/).map(m => { return lib_conv.toString(m,'').trim() }).filter(f => !lib_conv.isEmpty(f))
        let table_list_duplicates = lib_conv.duplicates(table_list)
        if (table_list_duplicates.length > 0) {
            throw new Error (lib_conv.format('in sql tables result "{0}" find no-unique table names "{1}"', [lib_conv.replaceAll(result_table,lib_os.EOL," "), table_list_duplicates.join(" ,")]))
        }
    }
    if (!lib_conv.isEmpty(result_postprocessor)) {
        postprocessor = new Function('callback_exec, props', result_postprocessor)
    }
    return {
        postprocessor: postprocessor,
        table_list: table_list
    }
}

/**
 * @param {string} sql_result_raw
 * @returns {function}
 */
function convert_js_postprocessor(sql_result_raw) {
    let sql_result = lib_conv.toString(sql_result_raw, '').trim()
    if (lib_conv.isEmpty(sql_result)) return new Function('callback_exec', 'return callback_exec.table_list.map(m => { return m.row_list })')
    if (sql_result.substring(0, 3) === '{t}') {
        sql_result = sql_result.substring(3, sql_result.length).trim()
        if (lib_conv.isEmpty(sql_result)) {
            throw new Error (lib_conv.format('in sql result "{0}" no find table names', sql_result_raw))
        }
        let table_list = sql_result.split(/[,;]/).map(m => { return lib_conv.toString(m,'').trim() }).filter(f => !lib_conv.isEmpty(f))
        let table_list_duplicates = lib_conv.duplicates(table_list)
        if (table_list_duplicates.length > 0) {
            throw new Error (lib_conv.format('in sql result "{0}" find no-unique table names "{1}"', [lib_conv.replaceAll(sql_result_raw,lib_os.EOL," "), table_list_duplicates.join(" ,")]))
        }

        return new Function('callback_exec', [
            lib_conv.format('if (callback_exec.table_list.length !== {0}) throw new Error("sql script return tables ".concat(callback_exec.table_list.length, ", but expected tables ", {0})) ', table_list.length),
            "return {",
            table_list.map((m,i) => { return lib_conv.format("{0}: callback_exec.table_list[{1}].row_list", [m, i])}),
            "}"
        ].join(lib_os.EOL))
    }
    if (sql_result.substring(0, 3) === '{f}') {
        sql_result = sql_result.substring(3, sql_result.length).trim()
        if (lib_conv.isEmpty(sql_result)) {
            throw new Error (lib_conv.format('in sql result "{0}" no find function body', sql_result_raw))
        }
        return new Function('callback_exec', sql_result)
    }
    throw new Error (lib_conv.format('sql result "{0}..." should start from "{t}" or "{f}"', lib_conv.replaceAll(sql_result_raw.substring(0, 10),lib_os.EOL," ")))
}

/**
 * @param {string} sql_result
 * @returns {string[]}
 */
function convert_sql_result_list (sql_result) {
    if (lib_conv.isEmpty(sql_result)) return []

    let sql_result_list = sql_result.split(/[,;]/).map(m => { return lib_conv.toString(m,'').trim() }).filter(f => !lib_conv.isEmpty(f))
    let sql_result_list_duplicates = lib_conv.duplicates(sql_result_list)
    if (sql_result_list_duplicates.length > 0) {
        throw new Error (lib_conv.format('in sql result "{0}" find no-unique properties "{1}"', [sql_result, sql_result_list_duplicates.join(" ,")]))
    }
    return sql_result_list
}

/**
 * @param {string} sql_param_notes
 * @param {type.sql_param[]} sql_param_list
 */
function convert_sql_param_note_list (sql_param_notes, sql_param_list) {
    sql_param_notes = string_bad_symbol_to_space(sql_param_notes).trim()

    /**
     * @typedef type_param_note
     * @property {string} name
     * @property {string} note
     */
    /** @type {type_param_note[]}*/
    let param_note_list = []
    /** @type {string[]}}*/
    let param_regular_list = []

    let param_name = ''
    let param_note = ''
    let is_from_regular = false

    while (0 === 0) {
        if (!lib_conv.isEmpty(param_name) && !lib_conv.isEmpty(param_note)) {
            param_note_list.push({name: param_name, note: param_note})
        } else if (!lib_conv.isEmpty(param_name) && is_from_regular === true && !param_regular_list.includes(param_name)) {
            param_regular_list.push(param_name)
        }

        if (sql_param_notes.length <= 0) break

        param_name = ''
        param_note = ''
        is_from_regular = false

        if ([",",";"].includes(sql_param_notes.substring(0,1))) {
            sql_param_notes = sql_param_notes.substring(1, sql_param_notes.length).trim()
        }

        let index_param_name = sql_param_notes.indexOf(' ')
        if (index_param_name > 0) {
            param_name = sql_param_notes.substring(0, index_param_name).trim()
            sql_param_notes = sql_param_notes.substring(index_param_name, sql_param_notes.length).trim()
        } else {
            param_name = sql_param_notes
            sql_param_notes = ''
        }

        if (sql_param_notes.length <= 0) break

        let quote = ''
        if (sql_param_notes.substring(0, 1) === '"') {
            quote = '"'
        } else if (sql_param_notes.substring(0, 1) === "'") {
            quote = "'"
        } else if (sql_param_notes.substring(0, 1) === "*") {
            is_from_regular = true
        }

        if (!lib_conv.isEmpty(quote)) {
            let index_quote = sql_param_notes.indexOf(quote, 1)
            if (index_quote < 0) break
            param_note = sql_param_notes.substring(1, index_quote).trim()
            sql_param_notes = sql_param_notes.substring(index_quote + 1, sql_param_notes.length).trim()
        } else if (is_from_regular === true) {
            sql_param_notes = sql_param_notes.substring(1, sql_param_notes.length).trim()
            if (sql_param_notes.length > 0 && ![";", ","].includes(sql_param_notes.substring(0, 1))) {
                break
            }
        } else {
            let index_separator1 = sql_param_notes.indexOf(",")
            let index_separator2 = sql_param_notes.indexOf(";")
            if (index_separator1 < 0 && index_separator2 < 0) {
                param_note = sql_param_notes
                sql_param_notes = ''
            } else {
                let index_separator = (index_separator1 >= 0 && index_separator2 < 0 ? index_separator1 :
                    (index_separator1 < 0 && index_separator2 >= 0 ? index_separator2 : Math.min(index_separator1, index_separator2))
                )
                param_note = sql_param_notes.substring(0, index_separator).trim()
                sql_param_notes = sql_param_notes.substring(index_separator, sql_param_notes.length).trim()
            }
        }
    }

    param_note_list.forEach(param_note => {
        let table_column_index = param_note.name.indexOf(".")
        if (table_column_index < 0) {
            sql_param_list.filter(f => f.type === 'scalar' && lib_conv.equal(f.scalar.name, param_note.name)).forEach(scalar => {
                scalar.scalar.description = [scalar.scalar.description, param_note.note].filter(f => !lib_conv.isEmpty(f)).join("; ")
            })
            sql_param_list.filter(f => f.type === 'table' && lib_conv.equal(f.table.name, param_note.name)).forEach(table => {
                table.table.description = [table.table.description, param_note.note].filter(f => !lib_conv.isEmpty(f)).join("; ")
            })
        } else {
            let table_name = param_note.name.substring(0, table_column_index).trim()
            let column_name = param_note.name.substring(table_column_index + 1, param_note.name.length).trim()
            sql_param_list.filter(f => f.type === 'table' && lib_conv.equal(f.table.name, table_name)).forEach(table => {
                table.table.column_list.filter(f => lib_conv.equal(f.name, column_name)).forEach(column => {
                    column.description = [column.description, param_note.note].filter(f => !lib_conv.isEmpty(f)).join("; ")
                })
            })
        }
    })

    param_regular_list.forEach(param_name => {
        let table_column_index = param_name.indexOf(".")
        if (table_column_index < 0) {
            sql_param_list.filter(f => f.type === 'scalar' && lib_conv.equal(f.scalar.name, param_name) && lib_conv.isEmpty(f.scalar.description) && !lib_conv.isEmpty(f.scalar.description)).forEach(scalar => {
                scalar.scalar.description = scalar.scalar.description_from_regular
            })
            sql_param_list.filter(f => f.type === 'table' && lib_conv.equal(f.table.name, param_name)).forEach(table => {
                if (lib_conv.isEmpty(table.table.description) && !lib_conv.isEmpty(table.table.description_from_regular)) {
                    table.table.description = table.table.description_from_regular
                }
                table.table.column_list.filter(f => lib_conv.isEmpty(f.description) && !lib_conv.isEmpty(f.description_from_regular)).forEach(column => {
                    column.description = column.description_from_regular
                })
            })
        } else {
            let table_name = param_name.substring(0, table_column_index).trim()
            let column_name = param_name.substring(table_column_index + 1, param_name.length).trim()
            sql_param_list.filter(f => f.type === 'table' && lib_conv.equal(f.table.name, table_name)).forEach(table => {
                table.table.column_list.filter(f => lib_conv.equal(f.name, column_name) && lib_conv.isEmpty(f.description) && !lib_conv.isEmpty(f.description_from_regular)).forEach(column => {
                    column.description = column.description_from_regular
                })
            })
        }
    })
}

/**
 * @param {string} column_name
 * @param {string} default_database
 * @param {string} default_schema
 * @returns {type.regular_table_replacement}
 */
function parse_regular_table_name(column_name, default_database, default_schema) {
    let name = column_name.substring(1, column_name.length).trim()
    if (name.length <= 0) {
        throw new Error(lib_conv.format('bad table name "{0}"', column_name))
    }
    let strong = true
    let database = default_database
    let schema = 'dbo'
    let table = ''
    if (name.substring(0, 1) === '*') {
        name = name.substring(1, name.length).trim()
        strong = false
        if (name.length <= 0) {
            throw new Error(lib_conv.format('bad table name "{0}"', column_name))
        }
    }
    let full_table_name = name.split('.')
    if (full_table_name.length === 1) {
        table = full_table_name[0].trim()
    } else if (full_table_name.length === 2) {
        table = full_table_name[1].trim()
        if (!lib_conv.isEmpty(full_table_name[0])) {
            schema = full_table_name[0].trim()
        }
    } else if (full_table_name.length === 3) {
        table = full_table_name[2].trim()
        if (!lib_conv.isEmpty(full_table_name[1])) {
            schema = full_table_name[1].trim()
        }
        if (!lib_conv.isEmpty(full_table_name[0])) {
            database = full_table_name[0].trim()
        }
    }
    table = lib_conv.border_del(table, '[', ']').toLowerCase()
    schema = lib_conv.border_del(schema, '[', ']').toLowerCase()
    database = lib_conv.border_del(database, '[', ']').toLowerCase()

    if (lib_conv.isEmpty(table) || lib_conv.isEmpty(schema) || lib_conv.isEmpty(database)) {
        throw new Error(lib_conv.format('failed parse table name from string "{0}"', column_name))
    }
    return {
        column_name: column_name,
        database: database,
        schema: schema,
        table: table,
        description: undefined,
        strong: strong,
        column_list: []
    }
}

/**
 * @param {string} name
 * @return {string}
 */
function js_name(name) {
    if (lib_conv.isEmpty(name)) return name
    let n = name.trim()
    if (lib_conv.isEmpty(n)) return name

    if (n.substring(0, 1) === '@') {
        return n.substring(1, n.length)
    }
    if (n.substring(0, 1) === '#') {
        n = n.substring(1, n.length)
        if (n.length > 0 && n.substring(0, 1) === '#') {
            n = n.substring(1, n.length)
        }
        return n
    }
    return name
}