//@ts-check
let lib_conv = require('viva-convert')
let app = require('./app.js')
let app_action = require('./app_action.js')
let app_sql_store = require('./app_sql_store.js')
const type = require('./@type.js')

/** @type {NodeJS.Timeout} */
let timer_store = undefined

/** @type {string} */
let last_max_lat_base64 = ''

exports.load_store_stop = load_store_stop
exports.load_store_start = load_store_start

/**
 * @param {function} [callback]
 */
function load_store_stop(callback) {
    if (!lib_conv.isAbsent(timer_store)) {
        clearTimeout(timer_store)
        timer_store = undefined
    }
    if (lib_conv.isFunction(callback)) {
        callback()
    }
}

/**
 * @param {app} app
 * @param {type.callback_error} [callback]
 */
function load_store_start(app, callback) {
    load_store_stop(() => {
        let timeout = app._private.sql_store.timeout_reload_sec
        if (timeout < 1) {
            timeout = 1
        }
        let first_timeout = timeout
        if (first_timeout > 0.1) {
            first_timeout = 0.1
        }

        let timeout_msec = timeout * 1000
        let first_timeout_msec = first_timeout * 1000

        timer_store = setTimeout(function tick() {
            app_sql_store.load_statistic(app, (error, max_lat_base64, action_count) => {
                if (!lib_conv.isAbsent(error)) {
                    if (lib_conv.isFunction(callback)) {
                        callback(error)
                    }
                    timer_store = setTimeout(tick, timeout_msec)
                    return
                }
                if (lib_conv.equal(last_max_lat_base64, max_lat_base64) && lib_conv.equal(app._private.action_render_list.length, action_count)) {
                    if (lib_conv.isFunction(callback)) {
                        callback(undefined)
                    }
                    timer_store = setTimeout(tick, timeout_msec)
                    return
                }
                last_max_lat_base64 = max_lat_base64

                app_sql_store.load(app, false, undefined, (error, action_raw_list) => {
                    if (!lib_conv.isAbsent(error)) {
                        if (lib_conv.isFunction(callback)) {
                            callback(error)
                        }
                        timer_store = setTimeout(tick, timeout_msec)
                        return
                    }
                    app_action.convert(app, action_raw_list, (error, action_render_list) => {
                        if (!lib_conv.isAbsent(error)) {
                            if (lib_conv.isFunction(callback)) {
                                callback(error)
                            }
                            timer_store = setTimeout(tick, timeout_msec)
                            return
                        }
                        app._private.action_render_list = action_render_list
                        if (lib_conv.isFunction(callback)) {
                            callback(undefined)
                        }
                        timer_store = setTimeout(tick, timeout_msec)
                    })
                })
            })
        }, first_timeout_msec)
    })
}


