//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_vmst_driver = require('vmst-driver')
const helper = require('vmst-helper')
const shared = require('vmst-shared')
const app = require('./app.js')
const action = require('./app_action.js')
const type = require('./@type.js')
const app_sql_shared = require('./app_sql_shared.js')

exports.connect = connect
exports.load_statistic = load_statistic
exports.load = load
exports.save = save

/**
 * @param {app} app
 * @param {type.callback_error} callback
 */
function connect(app, callback) {
    app_sql_shared.connect(app._private.sql_store.connection_options, app._private.app_name, app._private.scope_name, (error, sql) => {
        if (!lib_conv.isAbsent(error)) {
            callback(error)
            return
        }

        let schema = schema_beauty(app._private.sql_store.schema)
        let table = table_beauty(app._private.sql_store.table)

        let query_schema = [
            helper.depot_sch_schema(schema),
            helper.depot_sch_table(schema, table, 'action storage', [
                {name: 'rid', type: 'varchar', nullable: false, len_chars: 100, pk_position: 1, description: 'primary key'},
                {name: 'fdm', type: 'datetime', nullable: false, description: 'date/time create'},
                {name: 'ldm', type: 'datetime', nullable: false, description: 'date/time last modify'},
                {name: 'lat', type: 'timestamp', nullable: false},
                {name: 'tags', type: 'nvarchar', nullable: true, len_chars: 'max', description: 'tag list, format - {tag1}{tag2}'},
                {name: 'title', type: 'nvarchar', nullable: true, len_chars: 200},
                {name: 'note', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'sql_script', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'sql_param', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'sql_param_note', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'sql_lock', type: 'varchar', nullable: true, len_chars: 100},
                {name: 'sql_lock_wait', type: 'int', nullable: true},
                {name: 'sql_lock_message', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'result_table', type: 'nvarchar', nullable: true, len_chars: 'max'},
                {name: 'result_postprocessor', type: 'nvarchar', nullable: true, len_chars: 'max'},
            ], 'error'),
            helper.depot_sch_table(schema, lib_conv.format("{0}_eav", table), 'additional info for action storage', [
                {name: 'parent_rid', type: 'varchar', nullable: false, len_chars: 100, pk_position: 1, description: 'link to action'},
                {name: 'rid', type: 'varchar', nullable: false, len_chars: 100, pk_position: 2, description: 'key'},
                {name: 'data', type: 'nvarchar', nullable: false, len_chars: 'max', description: 'info'}
            ], 'error'),
            helper.depot_sch_foreign(
                schema, lib_conv.format("{0}_eav", table),
                schema, table,
                [{parent_column: 'rid', child_column: 'parent_rid'}],"cascade", "cascade"
            )
        ]

        sql.exec(query_schema, undefined, callback_exec => {
            if (callback_exec.type === 'end') {
                if (!lib_conv.isEmpty(callback_exec.end.error)) {
                    callback(callback_exec.end.error)
                    return
                }
                app._private.sql_store.sql = sql
                callback(undefined)
            }
        })
    })
}

/**
 * @param {string} schema
 * @returns {string}
 */
function schema_beauty(schema) {
    if (lib_conv.isEmpty(schema)) {
        return 'vmst'
    }
    let without_border = lib_conv.border_del(schema, '[', ']')
    if (lib_conv.isEmpty(without_border)) {
        return 'vmst'
    }
    return without_border
}

/**
 * @param {string} table
 * @returns {string}
 */
function table_beauty(table) {
    if (lib_conv.isEmpty(table)) {
        return 'action'
    }
    let without_border = lib_conv.border_del(table, '[', ']')
    if (lib_conv.isEmpty(without_border)) {
        return 'action'
    }
    return without_border
}

/**
 * @callback callback_load_store_stat
 * @param {Error} error
 * @param {string} max_lat_base64
 * @param {number} action_count
 *//**
 * @param {app} app
 * @param {callback_load_store_stat} callback
 */
function load_statistic(app, callback) {
    if (lib_conv.isAbsent(app._private.sql_store.sql)) {
        callback(new Error("Empty connection to MS SQL Server for store actions"), undefined, undefined)
        return
    }

    let schema = schema_beauty(app._private.sql_store.schema)
    let table = table_beauty(app._private.sql_store.table)

    let query = lib_conv.format("SELECT COUNT(*) cnt, MAX(lat) lat FROM [{0}].[{1}]", [schema, table])
    app._private.sql_store.sql.exec(query, undefined, callback_exec => {
        if (callback_exec.type !== 'end') return
        if (!lib_conv.isAbsent(callback_exec.end.error)) {
            callback(callback_exec.end.error, undefined, undefined)
            return
        }
        let row = callback_exec.end.table_list[0].row_list[0]
        let cnt = lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'cnt'), 0)
        let lat = lib_conv.findPropertyValueInObject(row, 'lat')

        callback(
            undefined,
            (Buffer.isBuffer(lat) ? lat.toString('base64') : ''),
            cnt
        )
    })
}

/**
 * @callback callback_load_store
 * @param {Error} error
 * @param {type.action_raw[]} action_raw_list
 *//**
 * @param {app} app
 * @param {boolean} load_eav
 * @param {string|string[]} [rids]
 * @param {callback_load_store} callback
 */
function load(app, load_eav, rids, callback) {
    if (lib_conv.isAbsent(app._private.sql_store.sql)) {
        callback(new Error("Empty connection to MS SQL Server for store actions"), undefined)
        return
    }

    let schema = schema_beauty(app._private.sql_store.schema)
    let table = table_beauty(app._private.sql_store.table)

    load_eav = lib_conv.toBool(load_eav, false)

    /** @type {string[]} */
    let rid_list = []
    if (!lib_conv.isEmpty(rids)) {
        if (Array.isArray(rids)) {
            rid_list = rids.map(m => { return lib_conv.toString(m) }).filter(f => !lib_conv.isEmpty(f))
        } else {
            rid_list.push(rids)
        }
    }

    let filter_by_rids = ""
    let filter_by_parent_rids = ""
    if (rid_list.length > 0) {
        filter_by_rids = lib_conv.format(" WHERE [rid] IN ({0}) ", rid_list.map(m => { return helper.helper_js_to_sql(m, 'varchar') }).join(","))
        filter_by_parent_rids = lib_conv.format(" WHERE [parent_rid] IN ({0}) ", rid_list.map(m => { return helper.helper_js_to_sql(m, 'varchar') }).join(","))
    }

    let query = [lib_conv.format("SELECT [rid], [fdm], [ldm], [lat], [tags], [title], [note], [sql_script], [sql_param], [sql_param_note], [sql_lock], [sql_lock_wait], [sql_lock_message], [result_table], [result_postprocessor] FROM [{0}].[{1}] {2} ORDER BY [rid]", [schema, table, filter_by_rids])]
    if (load_eav === true) {
        query.push(lib_conv.format("SELECT [parent_rid], [rid], [data] FROM [{0}].[{1}_eav] {2} ORDER BY [parent_rid], [rid]", [schema, table, filter_by_parent_rids]))
    }

    app._private.sql_store.sql.exec(query, undefined, callback_exec => {
        if (callback_exec.type !== 'end') return
        if (!lib_conv.isAbsent(callback_exec.end.error)) {
            callback(callback_exec.end.error, undefined)
            return
        }

        /**
         * @typedef type_eav
         * @property {string} parent_rid
         * @property {type.action_eav} eav
         */
        /** @type {type_eav[]}*/
        let action_eav_list = []
        if (load_eav === true) {
            callback_exec.end.table_list[1].row_list.forEach(row => {
                action_eav_list.push({
                    parent_rid: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'parent_rid'), ''),
                    eav: {
                        rid: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'rid'), ''),
                        data: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'data'), '')
                    }
                })
            })
        }

        /** @type  {type.action_raw[]}*/
        let action_raw_list = []
        callback_exec.end.table_list[0].row_list.forEach(row => {
            let rid = lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'rid'), '')
            action_raw_list.push({
                rid: rid,
                rid_origin: rid,
                title: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'title'), ''),
                note: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'note'), ''),
                sql_script: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'sql_script'), ''),
                sql_param: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'sql_param'), ''),
                sql_param_note: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'sql_param_note'), ''),
                sql_lock_key: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'sql_lock_key'), ''),
                sql_lock_wait: lib_conv.toInt(lib_conv.findPropertyValueInObject(row, 'sql_lock_wait')),
                sql_lock_message: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'sql_lock_message'), ''),
                tag_list: lib_conv.split(lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'tags'), ''), '{', '}', 'collapse_with_lower'),
                eav_list: action_eav_list.filter(f => f.parent_rid === rid).map(m => { return m.eav }),
                result_table: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'result_table'), ''),
                result_postprocessor: lib_conv.toString(lib_conv.findPropertyValueInObject(row, 'result_postprocessor'), '')
            })
        })

        callback(undefined, action_raw_list)
    })
}

/**
 * @param {app} app
 * @param {type.action_raw_edit[]} action_raw_list
 * @param {type.callback_error} callback
 */
function save(app, action_raw_list, callback) {
    if (lib_conv.isAbsent(app._private.sql_store.sql)) {
        callback(new Error("Empty connection to MS SQL Server for store actions"))
        return
    }

    let schema = schema_beauty(app._private.sql_store.schema)
    let table = table_beauty(app._private.sql_store.table)
    let union = " UNION ALL".concat(lib_os.EOL)

    let query = ["BEGIN TRAN", "DECLARE @now DATETIME; SET @now = GETDATE()"]

    let delete_list = action_raw_list.filter(f => f.edit_action === 'delete' && !lib_conv.isEmpty(f.rid_origin)).map(m => { return m.rid_origin.toLowerCase() })
    if (delete_list.length > 0) {
        query.push("DECLARE @delete TABLE([rid] VARCHAR(100) NOT NULL PRIMARY KEY)")
        query.push("INSERT INTO @delete([rid])")
        query.push(delete_list.map(m => { return "SELECT ".concat(helper.helper_js_to_sql(m,'varchar'))}).join(union))
        query.push(lib_conv.format("DELETE FROM [{0}].[{1}] WHERE rid IN (SELECT rid FROM @delete)", [schema, table]))
    }

    let update_rid_list = action_raw_list.filter(f => f.edit_action === 'save' && !lib_conv.isEmpty(f.rid) && !lib_conv.isEmpty(f.rid_origin) && !lib_conv.equal(f.rid_origin, f.rid)).map(m => { return {rid: m.rid, rid_origin: m.rid_origin }})
    if (update_rid_list.length > 0) {
        query.push("DECLARE @update_rid TABLE([rid] VARCHAR(100) NOT NULL PRIMARY KEY, [origin_rid] VARCHAR(100) NOT NULL)")
        query.push("INSERT INTO @update_rid([rid], [origin_rid])")
        query.push(update_rid_list.map(m => { return "SELECT ".concat(helper.helper_js_to_sql(m.rid,'varchar'), ",", helper.helper_js_to_sql(m.rid_origin, 'varchar'))}).join(union))
        query.push(lib_conv.format("UPDATE r SET r.[rid] = t.[rid] FROM [{0}].[{1}] r", [schema, table]))
        query.push("JOIN @update_rid t ON t.[origin_rid] = r.[rid]")
    }

    let save_list = action_raw_list.filter(f => f.edit_action === 'save' && !lib_conv.isEmpty(f.rid))
    if (save_list.length > 0) {

        save_list.filter(f => !lib_conv.isAbsent(f.tag_list)).forEach(item => {
            item.tag_list = item.tag_list.map(m => { return lib_conv.toString(m).trim() }).filter(f => !lib_conv.isEmpty(f))
            item.tag_list.forEach(tag => {
                if (tag.indexOf("{") >= 0 || tag.indexOf("}") >= 0 ) {
                    callback(new Error(lib_conv.format('in action "{0}" tag "{1}" has invalid character ("{" or "}")')))
                    return
                }
            })
        })

        query.push("DECLARE @edit TABLE([rid] VARCHAR(100) NOT NULL PRIMARY KEY,[tags] NVARCHAR(MAX),[title] NVARCHAR(200),[note] NVARCHAR(MAX),[sql_script] NVARCHAR(MAX),[sql_param] NVARCHAR(MAX),[sql_param_note] NVARCHAR(MAX),[sql_lock] VARCHAR(100),[sql_lock_wait] INT,[sql_lock_message] NVARCHAR(MAX), [result_table] NVARCHAR(MAX), [result_postprocessor] NVARCHAR(MAX))")
        query.push("DECLARE @edit_eav TABLE ([parent_rid] VARCHAR(100) NOT NULL,[rid] VARCHAR(100) NOT NULL,[data] NVARCHAR(MAX) NOT NULL, PRIMARY KEY([parent_rid],[rid]))")
        query.push("INSERT INTO @edit ([rid],[tags],[title],[note],[sql_script],[sql_param],[sql_param_note],[sql_lock],[sql_lock_wait],[sql_lock_message],[result_table],[result_postprocessor])")
        query.push(save_list.map(m => { return  lib_conv.format("SELECT {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", [
            helper.helper_js_to_sql(m.rid, 'varchar'),
            (lib_conv.isAbsent(m.tag_list) || m.tag_list.length <= 0 ? 'NULL' : helper.helper_js_to_sql("".concat("{",m.tag_list.join("}{"),"}" ),'nvarchar')),
            helper.helper_js_to_sql(m.title, 'nvarchar'),
            helper.helper_js_to_sql(m.note, 'nvarchar'),
            helper.helper_js_to_sql(m.sql_script, 'nvarchar'),
            helper.helper_js_to_sql(m.sql_param, 'nvarchar'),
            helper.helper_js_to_sql(m.sql_param_note, 'nvarchar'),
            helper.helper_js_to_sql(m.sql_lock_key, 'varchar'),
            helper.helper_js_to_sql(m.sql_lock_wait, 'int'),
            helper.helper_js_to_sql(m.sql_lock_message, 'nvarchar'),
            helper.helper_js_to_sql(m.result_table, 'nvarchar'),
            helper.helper_js_to_sql(m.result_postprocessor, 'nvarchar')
        ]) }).join(union))

        /**
         * @typedef type_save_list_eav
         * @property {string} parent_rid
         * @property {string} rid
         * @property {string} data
        *//** @type {type_save_list_eav[]} */
        let save_list_eav = []
        save_list.filter(f => !lib_conv.isAbsent(f.eav_list)).forEach(item => {
            item.eav_list.filter(f => !lib_conv.isEmpty(f.data)).forEach(eav => {
                save_list_eav.push({
                    parent_rid: item.rid,
                    rid: eav.rid,
                    data: eav.data
                })
            })
        })
        if (save_list_eav.length > 0) {
            query.push("INSERT INTO @edit_eav ([parent_rid],[rid],[data])")
            query.push(save_list_eav.map(m => { return  lib_conv.format("SELECT {0},{1},{2}", [
                helper.helper_js_to_sql(m.parent_rid, 'varchar'),
                helper.helper_js_to_sql(m.rid, 'varchar'),
                helper.helper_js_to_sql(m.data, 'nvarchar')
            ]) }).join(union))
        }

        query.push("UPDATE r SET r.[ldm]=@now,r.[tags]=t.[tags],r.[title]=t.[title],r.[note]=t.[note],r.[sql_script]=t.[sql_script],r.[sql_param]=t.[sql_param],r.[sql_param_note]=t.[sql_param_note],r.[sql_lock]=t.[sql_lock],r.[sql_lock_wait]=t.[sql_lock_wait],r.[sql_lock_message]=t.[sql_lock_message],r.[result_table]=t.[result_table],r.[result_postprocessor]=t.[result_postprocessor]")
        query.push(lib_conv.format("FROM [{0}].[{1}] r", [schema, table]))
        query.push("JOIN @edit t ON t.[rid] = r.[rid]")
        query.push("WHERE (r.[tags] IS NULL AND t.[tags] IS NOT NULL) OR (r.[tags] IS NOT NULL AND t.[tags] IS NULL) OR r.[tags]<>t.[tags]")
        query.push("   OR (r.[title] IS NULL AND t.[title] IS NOT NULL) OR (r.[title] IS NOT NULL AND t.[title] IS NULL) OR r.[title]<>t.[title]")
        query.push("   OR (r.[note] IS NULL AND t.[note] IS NOT NULL) OR (r.[note] IS NOT NULL AND t.[note] IS NULL) OR r.[note]<>t.[note]")
        query.push("   OR (r.[sql_script] IS NULL AND t.[sql_script] IS NOT NULL) OR (r.[sql_script] IS NOT NULL AND t.[sql_script] IS NULL) OR r.[sql_script]<>t.[sql_script]")
        query.push("   OR (r.[sql_param] IS NULL AND t.[sql_param] IS NOT NULL) OR (r.[sql_param] IS NOT NULL AND t.[sql_param] IS NULL) OR r.[sql_param]<>t.[sql_param]")
        query.push("   OR (r.[sql_param_note] IS NULL AND t.[sql_param_note] IS NOT NULL) OR (r.[sql_param_note] IS NOT NULL AND t.[sql_param_note] IS NULL) OR r.[sql_param_note]<>t.[sql_param_note]")
        query.push("   OR (r.[sql_lock] IS NULL AND t.[sql_lock] IS NOT NULL) OR (r.[sql_lock] IS NOT NULL AND t.[sql_lock] IS NULL) OR r.[sql_lock]<>t.[sql_lock]")
        query.push("   OR (r.[sql_lock_wait] IS NULL AND t.[sql_lock_wait] IS NOT NULL) OR (r.[sql_lock_wait] IS NOT NULL AND t.[sql_lock_wait] IS NULL) OR r.[sql_lock_wait]<>t.[sql_lock_wait]")
        query.push("   OR (r.[sql_lock_message] IS NULL AND t.[sql_lock_message] IS NOT NULL) OR (r.[sql_lock_message] IS NOT NULL AND t.[sql_lock_message] IS NULL) OR r.[sql_lock_message]<>t.[sql_lock_message]")
        query.push("   OR (r.[result_table] IS NULL AND t.[result_table] IS NOT NULL) OR (r.[result_table] IS NOT NULL AND t.[result_table] IS NULL) OR r.[result_table]<>t.[result_table]")
        query.push("   OR (r.[result_postprocessor] IS NULL AND t.[result_postprocessor] IS NOT NULL) OR (r.[result_postprocessor] IS NOT NULL AND t.[result_postprocessor] IS NULL) OR r.[result_postprocessor]<>t.[result_postprocessor]")
        query.push(lib_conv.format("INSERT INTO [{0}].[{1}]", [schema, table]))
        query.push("([fdm],[ldm],[tags],[title],[note],[sql_script],[sql_param],[sql_param_note],[sql_lock],[sql_lock_wait],[sql_lock_message],[result_table],[result_postprocessor])")
        query.push("SELECT @now,@now,t.[tags],t.[title],t.[note],t.[sql_script],t.[sql_param],t.[sql_param_note],t.[sql_lock],t.[sql_lock_wait],t.[sql_lock_message],t.[result_table],t.[result_postprocessor]")
        query.push("FROM @edit t")
        query.push(lib_conv.format("LEFT JOIN [{0}].[{1}] r ON r.rid = t.rid", [schema, table]))
        query.push("WHERE r.rid IS NULL")

        query.push(lib_conv.format("DELETE FROM r FROM [{0}].[{1}_eav] r", [schema, table]))
        query.push("LEFT JOIN @edit_eav t ON t.parent_rid = r.parent_rid AND t.rid = r.rid")
        query.push("WHERE r.parent_rid IN (select rid FROM @edit) AND t.parent_rid IS NULL")
        query.push(lib_conv.format("UPDATE r SET r.data = t.data FROM [{0}].[{1}_eav] r", [schema, table]))
        query.push("JOIN @edit_eav t ON t.parent_rid = r.parent_rid AND t.rid = r.rid")
        query.push("WHERE r.parent_rid IN (select rid FROM @edit) AND r.data <> t.data")
        query.push(lib_conv.format("INSERT INTO [{0}].[{1}_eav] (parent_rid, rid, data)", [schema, table]))
        query.push("SELECT t.parent_rid, t.rid, t.data FROM @edit_eav t")
        query.push(lib_conv.format("LEFT JOIN [{0}].[{1}_eav] r ON r.parent_rid = t.parent_rid AND r.rid = t.rid", [schema, table]))
        query.push("WHERE r.parent_rid IS NULL")
    }

    query.push("COMMIT")

    app._private.sql_store.sql.exec(query.join(lib_os.EOL), undefined, callback_exec => {
        if (callback_exec.type !== 'end') return
        let q = callback_exec.end.get_beauty_query('actual')
        console.log(q)
        callback(callback_exec.end.error)
    })

}